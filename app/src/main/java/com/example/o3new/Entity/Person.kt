package com.example.o3.models

data class Person(
    val address: String,
    val description: String,
    val firstName: String,
    val id: Int,
    val internetAccountPassword: String,
    val job: String,
    val lastName: String,
    val nationalCode: String,
    val phone: String,
    val photo: Any,
    val userId: Int,
    val wallet: Int
)