package com.example.o3.models

data class User(
    val id: Int,
    val isActive: Boolean,
    val isEnableEmail: Boolean,
    val person: Person,
    val status: Int,
    val type: String,
    val username: String,
    val verification: Boolean
)