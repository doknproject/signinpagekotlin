package com.example.o3.models

data class SignupBody(
    val username: String,
    val email : String,
    val password: String,
)