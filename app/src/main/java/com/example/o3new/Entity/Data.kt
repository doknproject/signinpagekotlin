package com.example.o3.models

data class Data(
    val token: String,
    val user: User
)