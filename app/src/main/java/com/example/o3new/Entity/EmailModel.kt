package com.example.o3.models

data class EmailModel(
    val callbackURL: String,
    val username: String
)