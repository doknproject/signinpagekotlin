package com.example.o3.models

data class SignInBody(
    val password: String,
    val type: Int,
    val username: String
)