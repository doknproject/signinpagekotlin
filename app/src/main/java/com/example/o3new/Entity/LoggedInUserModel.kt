package com.example.o3.models

data class LoggedInUserModel(
    val `data`: Data,
    val message: String,
    val status: Int
)