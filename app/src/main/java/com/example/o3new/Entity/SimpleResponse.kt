package com.example.o3.models

data class SimpleResponse(
    val message: String,
    val status: Int
)