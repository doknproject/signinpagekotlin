package com.example.o3.viewModels.API.repository

import android.util.Log
import com.example.o3.viewModels.API.ApiInterface.RetrofitInstance
import com.example.o3.models.EmailModel
import com.example.o3.models.LoggedInUserModel
import com.example.o3.models.SignInBody
import com.example.o3.models.SignupBody
import com.example.o3.models.SimpleResponse

import okhttp3.ResponseBody
import retrofit2.HttpException
import retrofit2.Response

class Repository {



    suspend fun register(body: SignupBody): Response<SimpleResponse>{

            return RetrofitInstance.api.register(body)


    }

    suspend fun login(body: SignInBody): Response<LoggedInUserModel>{

        return RetrofitInstance.api.login(body)


    }

}