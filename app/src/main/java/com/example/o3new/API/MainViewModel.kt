package com.example.o3.viewModels.API

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.o3.models.EmailModel
import com.example.o3.models.LoggedInUserModel
import com.example.o3.models.SignInBody
import com.example.o3.models.SignupBody
import com.example.o3.models.SimpleResponse

import com.example.o3.viewModels.API.repository.Repository
import kotlinx.coroutines.launch
import retrofit2.Response

class MainViewModel(private val repository: Repository) : ViewModel() {



    var viewModelSignupResponse: MutableLiveData<Response<SimpleResponse>> = MutableLiveData()
    fun register(body: SignupBody, showPopup: MutableState<Boolean>) {
        viewModelScope.launch {
            try {
                val response: Response<SimpleResponse> = repository.register(body)
                viewModelSignupResponse.value = response
            } catch (e: Exception) {
                Log.d("register -->", "${e.message} ")
                showPopup.value = false
            }
        }
    }

    var viewModelSignInResponse: MutableLiveData<Response<LoggedInUserModel>> =
        MutableLiveData()

    fun login(body: SignInBody, showPopup: MutableState<Boolean>) {
        viewModelScope.launch {
            try {
                val response: Response<LoggedInUserModel> = repository.login(body)
                viewModelSignInResponse.value = response
            } catch (e: Exception) {
                Log.d("login -->", "${e.message} ")
                showPopup.value = false
            }
        }
    }










}