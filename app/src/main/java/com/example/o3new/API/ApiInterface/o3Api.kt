package com.example.o3.viewModels.API.ApiInterface


import com.example.o3.models.EmailModel
import com.example.o3.models.LoggedInUserModel
import com.example.o3.models.SignInBody
import com.example.o3.models.SignupBody
import com.example.o3.models.SimpleResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.PUT
//DAO
interface o3Api {


    @POST("register")
    suspend fun register(
        @Body signupBody: SignupBody
    ):Response<SimpleResponse>

    @POST("login")
    suspend fun login(
        @Body signInBody: SignInBody
    ):Response<LoggedInUserModel>


}