package com.example.o3.viewModels.API.ApiInterface


import com.example.o3.viewModels.API.util.Constants.Companion.BASE_URL
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object RetrofitInstance {
    private  val  retrofit by lazy {
        Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build()
    }
    val api: o3Api by lazy {
        retrofit.create(o3Api::class.java)
    }

}