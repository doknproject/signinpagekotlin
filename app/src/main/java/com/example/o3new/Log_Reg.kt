package com.example.o3new

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.material3.contentColorFor
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LifecycleOwner
import com.example.o3.viewModels.API.MainViewModel
import com.example.o3.viewModels.API.repository.Repository

@Composable
fun logreg(
    modifier: Modifier = Modifier,
    owner: LifecycleOwner,
    passwordValidationRegex: Regex = Regex("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).+\$")
) {
    val localConfiguration = LocalConfiguration.current
    val screenWidth = with(LocalDensity.current) {
        localConfiguration.screenWidthDp.dp.toPx()
    }
    val screenHeight = with(LocalDensity.current) {
        localConfiguration.screenHeightDp.dp.toPx()
    }
    var inputText by remember { mutableStateOf("") }
    var inputText2 by remember { mutableStateOf("") }
    var inputText3 by remember { mutableStateOf("") }
    var isFocused by remember { mutableStateOf(false) }
    var passwordVisible by remember { mutableStateOf(false) }
    var passwordVisible2 by remember { mutableStateOf(false) }
    var passwordError by remember { mutableStateOf("") }

    val repository = Repository()
    val VM = MainViewModel(repository = repository)
    VM.viewModelSignupResponse.observe(owner) { respose ->

    }
    DisposableEffect(Unit) {
        onDispose {
            isFocused = false
        }
    }
    Column(
        Modifier
            .fillMaxSize()
            .systemBarsPadding()
            .background(Color.White)
    ) {
        Box(
            Modifier
                .fillMaxWidth()
                .height(height = 295.dp)
                .offset(y = -5.dp)
        ) {
            Image(
                painter = painterResource(id = R.drawable.p1),
                contentDescription = null,
                Modifier.fillMaxSize()
                //alignment = Alignment.Center
            )
        }
        Spacer(modifier = Modifier.height(5.dp))

        Row(
            modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceEvenly
        ) {

            TextField(
                value = inputText,
                onValueChange = { enteredText -> inputText = enteredText },
                label = { Text("Email") },
                placeholder = {
                    if (isFocused && inputText.isEmpty()) {
                        Text(text = "  Enter Your Email! ")
                    } else {
                        Text(text = "")
                    }
                },
                leadingIcon = {
                    Icon(
                        painter = painterResource(id = R.drawable.left_icon),
                        contentDescription = "Email Icon",
                        modifier = Modifier
                            .size(20.dp)
                    )
                },
                modifier = Modifier
                    .onFocusChanged { focusState ->
                        isFocused = focusState.isFocused
                    },
                colors = TextFieldDefaults.colors(
                    unfocusedIndicatorColor = Color.Red,
                    focusedIndicatorColor = Color.Green,
                    focusedContainerColor = Color.Cyan,
                    unfocusedContainerColor = Color.White,
                    focusedLabelColor = Color.Gray,
                    unfocusedLabelColor = Color.Blue,
                    focusedTextColor = Color.Black,
                    unfocusedTextColor = Color.Yellow
                ),
            )

        }
        Spacer(modifier = Modifier.height(5.dp))
        //2
        Row(
            modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceEvenly
        ) {

            TextField(
                value = inputText2,
                onValueChange = { enteredText -> inputText2 = enteredText },
                label = { Text("Password") },
                placeholder = {
                    if (isFocused && inputText.isEmpty()) {
                        Text(text = "  Enter Your Password! ")
                    } else {
                        Text(text = "")
                    }
                },
                visualTransformation = if (passwordVisible) VisualTransformation.None else PasswordVisualTransformation(),
                leadingIcon = {
                    Icon(
                        painter = painterResource(id = R.drawable.left_icon),
                        contentDescription = "Email Icon",
                        modifier = Modifier
                            .size(20.dp)
                            .clickable { passwordVisible = !passwordVisible }
                    )
                },
                modifier = Modifier
                    .onFocusChanged { focusState ->
                        isFocused = focusState.isFocused
                    },
                colors = TextFieldDefaults.colors(
                    unfocusedIndicatorColor = Color.Red,
                    focusedIndicatorColor = Color.Green,
                    focusedContainerColor = Color.Cyan,
                    unfocusedContainerColor = Color.White,
                    focusedLabelColor = Color.Gray,
                    unfocusedLabelColor = Color.Blue,
                    focusedTextColor = Color.Black,
                    unfocusedTextColor = Color.Yellow
                ),
            )

        }
        Spacer(modifier = Modifier.height(5.dp))
        //3
        Row(
            modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceEvenly
        ) {

            TextField(
                value = inputText3,
                onValueChange = { enteredText -> inputText3 = enteredText },
                label = { Text("Again Password") },
                placeholder = {
                    if (isFocused && inputText.isEmpty()) {
                        Text(text = "  Enter Your Password Again! ")
                    } else {
                        Text(text = "")
                    }
                },
                visualTransformation = if (passwordVisible2) VisualTransformation.None else PasswordVisualTransformation(),
                leadingIcon = {
                    Icon(
                        painter = painterResource(id = R.drawable.left_icon),
                        contentDescription = "Email Icon",
                        modifier = Modifier
                            .size(20.dp)
                            .clickable { passwordVisible2 = !passwordVisible2 }
                    )
                },
                modifier = Modifier
                    .onFocusChanged { focusState ->
                        isFocused = focusState.isFocused
                    },
                colors = TextFieldDefaults.colors(
                    unfocusedIndicatorColor = Color.Red,
                    focusedIndicatorColor = Color.Green,
                    focusedContainerColor = Color.Cyan,
                    unfocusedContainerColor = Color.White,
                    focusedLabelColor = Color.Gray,
                    unfocusedLabelColor = Color.Blue,
                    focusedTextColor = Color.Black,
                    unfocusedTextColor = Color.Yellow
                ),
            )

        }

        Spacer(modifier = Modifier.height(15.dp))

        Row(
            Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceEvenly
        ) {

            Button(
                modifier = Modifier
                    .size(300.dp, 40.dp),
                onClick = {
                    if (inputText2 == inputText3 && passwordValidationRegex.matches(inputText2)) {
                        // Password validation passed, proceed with the next step
                        passwordError = "Sucsuss"
                        // TODO: Proceed with the next step
                    } else {
                        // Password validation failed, show the error
                        passwordError = when {
                            inputText2 != inputText3 -> "Passwords do not match"
                            !passwordValidationRegex.matches(inputText2) -> "Password must contain at least one uppercase letter, one lowercase letter, and one number"
                            else -> "please try again later"
                        }
                    }
                },

                colors = ButtonDefaults.buttonColors(containerColor = Color.Green)
            ) {
                Text(text = "Next Step")
            }

        }
        if (passwordError.isNotEmpty()) {
            Text(
                text = passwordError,
                color = Color.Red,
                modifier = Modifier.padding(top = 8.dp)
            )

        }
    }
}